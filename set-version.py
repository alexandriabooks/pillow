#!/usr/bin/env python -u

# Copyright (c) 2021

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# This script is built to run on a variety of systems where a Python install is
# present and is used to set the version number of projects pre-build.

# Changelog:
# Version 2021.1.28
# Added:
# - Can write a custom formatted version out to a text file.
#
# Version 2021.1.25
# Changed:
# - Will optionally drop the end of version strings that are zero.
#
# Version 2021.1.24
# Added:
# - Initial support for Conda
# - Initial support for Python const
# - Initial support for Poetry pyproject.toml
# - Initial support for Maven pom.xml
# - Initial support for NPM

from optparse import OptionParser
import re
import logging
import xml.etree.ElementTree as ET
import json
import datetime
import subprocess

logger = logging.getLogger(__name__)
VERSION = "2021.1.28"


def version_concat(*parts, minimum_parts=2):
    if not parts:
        return ""
    add_all = False
    result = []
    for i in range(len(parts) - 1, 0, -2):
        number = str(parts[i])
        if i > 0:
            sep = parts[i - 1]
        else:
            sep = ""
        if not add_all:
            if i < minimum_parts * 2:
                add_all = True
            elif number and number != "0":
                add_all = True
        if add_all:
            result.insert(0, number)
            result.insert(0, sep)
    result.insert(0, parts[0])
    return "".join(result)


def set_conda_version(conda_meta_file, version, dry_run):
    (
        major_version,
        minor_version,
        patch_version,
        build_version,
        revision,
        dev_version
    ) = version

    if dev_version:
        version_string = version_concat(
            major_version,
            ".",
            minor_version,
            ".",
            patch_version,
            ".dev",
            build_version
        )
    else:
        version_string = version_concat(
            major_version,
            ".",
            minor_version,
            ".",
            patch_version,
            ".",
            build_version
        )
    if revision:
        version_string += "+%s" % (revision)

    logger.info("Setting conda version in %s to %s", filename, version_string)

    with open(conda_meta_file, "r", encoding="utf8") as fp:
        meta_yaml = fp.read()
    meta_yaml = re.sub(
        r'^{%\s*set\s+version\s*=\s*"[^"]*"\s*%}',
        '{{%% set version = "%s" %%}}' % (version_string),
        meta_yaml,
        flags=re.MULTILINE
    )
    if dry_run:
        logging.info(
            "Would write to %s:\n%s",
            conda_meta_file,
            meta_yaml
        )
    else:
        with open(conda_meta_file, "w", encoding="utf8") as fp:
            fp.write(meta_yaml)


def set_python_file_version(python_file, version, dry_run):
    (
        major_version,
        minor_version,
        patch_version,
        build_version,
        revision,
        dev_version
    ) = version

    if dev_version:
        version_string = version_concat(
            major_version,
            ".",
            minor_version,
            ".",
            patch_version,
            ".dev",
            build_version
        )
    else:
        version_string = version_concat(
            major_version,
            ".",
            minor_version,
            ".",
            patch_version,
            ".",
            build_version
        )
    if revision:
        version_string += "+%s" % (revision)

    logger.info("Setting python constant version in %s to %s", filename, version_string)

    with open(python_file, "r", encoding="utf8") as fp:
        python_contents = fp.read()
    python_contents = re.sub(
        r'^VERSION\s*=\s*"[^"]*"',
        'VERSION = "%s"' % (version_string),
        python_contents,
        flags=re.MULTILINE
    )
    if dry_run:
        logging.info(
            "Would write to %s:\n%s",
            python_file,
            python_contents
        )
    else:
        with open(python_file, "w", encoding="utf8") as fp:
            fp.write(python_contents)


def set_poetry_pyproject_toml_version(pyproject_toml_file, version, dry_run):
    (
        major_version,
        minor_version,
        patch_version,
        build_version,
        revision,
        dev_version
    ) = version

    if dev_version:
        version_string = version_concat(
            major_version,
            ".",
            minor_version,
            ".",
            patch_version,
            ".dev",
            build_version
        )
    else:
        version_string = version_concat(
            major_version,
            ".",
            minor_version,
            ".",
            patch_version,
            ".",
            build_version
        )
    if revision:
        version_string += "+%s" % (revision)

    logger.info("Setting poetry version in %s to %s", filename, version_string)

    with open(pyproject_toml_file, "r", encoding="utf8") as fp:
        pyproject_toml = fp.read()
    pyproject_toml = re.sub(
        r'^version\s*=\s*"[^"]*"',
        'version = "%s"' % (version_string),
        pyproject_toml,
        flags=re.MULTILINE
    )
    if dry_run:
        logging.info(
            "Would write to %s:\n%s",
            pyproject_toml_file,
            pyproject_toml
        )
    else:
        with open(pyproject_toml_file, "w", encoding="utf8") as fp:
            fp.write(pyproject_toml)


def set_maven_pom_version(pom_xml_file, version, dry_run):
    (
        major_version,
        minor_version,
        patch_version,
        build_version,
        revision,
        dev_version
    ) = version
    version_string = version_concat(
        major_version,
        ".",
        minor_version,
        ".",
        patch_version,
        ".",
        build_version
    )
    if dev_version:
        version_string += "-SNAPSHOT" % (revision)

    logger.info("Setting Maven POM version in %s to %s", filename, version_string)

    tree = ET.ElementTree(file=pom_xml_file)
    root = tree.getroot()

    ET.register_namespace("", "http://maven.apache.org/POM/4.0.0")

    for child in root:
        if child.tag.endswith("version"):
            child.text = version_string
            break
    else:
        version_tag = ET.Element("version")
        version_tag.text = version_string
        root.append(version_tag)

    if dry_run:
        logging.info(
            "Would write to %s:\n%s",
            pom_xml_file,
            ET.tostring(root, encoding='utf8', method='xml').decode("utf8")
        )
    else:
        with open(pom_xml_file, "wb") as fp:
            tree.write(fp)


def set_npm_package_version(package_json_file, version, dry_run):
    (
        major_version,
        minor_version,
        patch_version,
        build_version,
        revision,
        dev_version
    ) = version

    if dev_version:
        version_string = version_concat(
            major_version,
            ".",
            minor_version,
            ".",
            patch_version,
            "alpha+",
            build_version
        )
    else:
        version_string = version_concat(
            major_version,
            ".",
            minor_version,
            ".",
            patch_version,
            "+",
            build_version
        )

    if revision:
        version_string += "-%s" % (revision)

    logger.info("Setting NPM package version in %s to %s", filename, version_string)

    with open(package_json_file, "rb") as handle:
        package = json.load(handle)

    package["version"] = version_string
    if dry_run:
        logging.info(
            "Would write to %s:\n%s",
            package_json_file,
            json.dumps(package, indent=2)
        )
    else:
        with open(package_json_file, "w", encoding="utf8") as fp:
            json.dump(package, fp, indent=2)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    parser = OptionParser()
    parser.add_option(
        "--dry-run",
        action="store_true",
        dest="dry_run",
        default=False,
        help="Do not make any changes."
    )
    parser.add_option(
        "--today",
        action="store_true",
        dest="today_date_version",
        default=False,
        help="Use today's date as a version number: year.month.day"
    )
    parser.add_option(
        "--git",
        action="store_true",
        dest="use_git_rev",
        default=False,
        help="Incorperate git's revision in the version number"
    )
    parser.add_option(
        "--major",
        action="store",
        dest="major_version",
        help="Sets the major version",
        metavar="VERSION"
    )
    parser.add_option(
        "--minor",
        action="store",
        dest="minor_version",
        help="Sets the minor version",
        metavar="VERSION"
    )
    parser.add_option(
        "--patch",
        action="store",
        dest="patch_version",
        help="Sets the patch version",
        metavar="VERSION"
    )
    parser.add_option(
        "--build",
        action="store",
        dest="build_version",
        help="Sets the build version",
        metavar="VERSION"
    )
    parser.add_option(
        "--revision",
        action="store",
        dest="revision",
        help="Sets the reversion",
        metavar="VERSION"
    )
    parser.add_option(
        "--dev",
        action="store_true",
        dest="dev_version",
        help="Sets the version as a dev version"
    )
    parser.add_option(
        "--conda-file",
        action="append",
        dest="conda_file",
        help="Updates the version in the conda meta.yaml file",
        metavar="FILE"
    )
    parser.add_option(
        "--python-const-file",
        action="append",
        dest="python_const_file",
        help="Updates the 'VERSION=' constant in a python file",
        metavar="FILE"
    )
    parser.add_option(
        "--poetry-toml-file",
        action="append",
        dest="poetry_toml_file",
        help="Updates the 'version =' parameter in a pyproject.toml file",
        metavar="FILE"
    )
    parser.add_option(
        "--maven-pom-file",
        action="append",
        dest="maven_pom_file",
        help="Updates the <version> tag in a maven POM",
        metavar="FILE"
    )
    parser.add_option(
        "--npm-package-json-file",
        action="append",
        dest="npm_package_json",
        help="Updates the version property in a NPM package.json",
        metavar="FILE"
    )
    parser.add_option(
        "--version-txt-file",
        action="append",
        dest="version_txt_file",
        help="Writes the version out to a text file",
        nargs=2,
        metavar="PATTERN FILE"
    )

    (options, args) = parser.parse_args()

    major_version = 0
    minor_version = 0
    patch_version = 0
    build_version = 0
    revision = ""

    if options.today_date_version:
        today = datetime.date.today()
        major_version = today.year
        minor_version = today.month
        patch_version = today.day

    if options.use_git_rev:
        revision = subprocess.check_output(
            ["git", "rev-parse", "--short=8", "HEAD"]
        ).decode("ascii").strip()

    if options.major_version:
        major_version = options.major_version

    if options.minor_version:
        minor_version = options.minor_version

    if options.patch_version:
        patch_version = options.patch_version

    if options.build_version:
        build_version = options.build_version

    if options.revision:
        revision = options.revision

    major_version = str(major_version)
    minor_version = str(minor_version)
    patch_version = str(patch_version)
    build_version = str(build_version)
    revision = str(revision)
    dev_version = bool(options.dev_version)

    version = (
        major_version,
        minor_version,
        patch_version,
        build_version,
        revision,
        dev_version
    )

    logger.info(
        (
            "Setting version: "
            "MAJOR: %s "
            "MINOR: %s "
            "PATCH: %s "
            "BUILD: %s "
            "REV: %s "
            "(dev build: %s)"
        ) % version
    )

    dry_run = options.dry_run
    if dry_run:
        logger.info("Running as dry-run...")

    if options.conda_file:
        for filename in options.conda_file:
            set_conda_version(filename, version, dry_run)

    if options.python_const_file:
        for filename in options.python_const_file:
            set_python_file_version(filename, version, dry_run)

    if options.poetry_toml_file:
        for filename in options.poetry_toml_file:
            set_poetry_pyproject_toml_version(filename, version, dry_run)

    if options.maven_pom_file:
        for filename in options.maven_pom_file:
            set_maven_pom_version(filename, version, dry_run)

    if options.npm_package_json:
        for filename in options.npm_package_json:
            set_npm_package_version(filename, version, dry_run)

    if options.version_txt_file:
        for pattern, filename in options.version_txt_file:
            with open(filename, "w", encoding="utf8") as handle:
                handle.write(pattern.format(
                    major=major_version,
                    minor=minor_version,
                    patch=patch_version,
                    build=build_version,
                    revision=revision
                ))
