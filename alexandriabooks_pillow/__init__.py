import typing
import logging

logger = logging.getLogger(__name__)
ALEXANDRIA_PLUGINS: typing.Dict[str, typing.Any] = {}

try:
    from .transcoder import PILImageTranscoderService

    ALEXANDRIA_PLUGINS["ImageTranscoderService"] = [PILImageTranscoderService]
except ImportError as e:
    logger.debug("Could not load pillow plugin", exc_info=e)
