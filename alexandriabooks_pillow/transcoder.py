import io

from PIL import Image

from alexandriabooks_core.model import ImageType
from alexandriabooks_core.services import ImageTranscoderService


class PILImageTranscoderService(ImageTranscoderService):
    def __init__(self, **kwargs):
        pass

    def __str__(self):
        return "Image Transcoder (Pillow)"

    async def transcode_image(
        self,
        src,
        desired_type,
        desired_size=None,
        max_size=None,
        greyscale=None,
        out_stream=None,
    ):
        output_type = None
        image_type = ImageType.to_image_type(desired_type)

        if image_type == ImageType.BMP:
            output_type = "BMP"
        elif image_type == ImageType.GIF:
            output_type = "GIF"
        elif image_type == ImageType.JPEG:
            output_type = "JPEG"
        elif image_type == ImageType.PNG:
            output_type = "PNG"
        elif image_type == ImageType.TIFF:
            output_type = "TIFF"
        elif image_type == ImageType.WEBP:
            output_type = "WEBP"
        else:
            return None

        im = Image.open(src)
        if greyscale:
            im = im.convert("L")
        else:
            im = im.convert("RGB")
        orig_width, orig_height = im.size
        if desired_size:
            desired_width, desired_height = desired_size

            if not desired_width:
                desired_width = orig_width

            if not desired_height:
                desired_height = orig_height
        else:
            desired_width = orig_width
            desired_height = orig_height

        if max_size:
            max_width, max_height = max_size
            if max_width and max_width < desired_width:
                desired_width = max_width
            if max_height and max_height < desired_height:
                desired_height = max_height

        if desired_width != orig_width or desired_height != orig_height:
            ratio = 1.0
            if desired_width < orig_width:
                ratio = min(ratio, desired_width / float(orig_width))
            else:
                ratio = max(ratio, desired_width / float(orig_width))

            if desired_height < orig_height:
                ratio = min(ratio, desired_height / float(orig_height))
            else:
                ratio = max(ratio, desired_height / float(orig_height))

            new_width = int(orig_width * ratio)
            new_height = int(orig_height * ratio)

            im = im.resize((new_width, new_height), resample=Image.BICUBIC)

        if im.mode in ("RGBA", "LA"):  # If alpha channel, fill alpha with black
            background = Image.new(im.mode[:-1], im.size, 0)
            background.paste(im, im.split()[-1])
            im = background

        if out_stream is not None:
            im.save(out_stream, output_type)
            return None
        else:
            out_stream = io.BytesIO()
            im.save(out_stream, output_type)
            out_stream.seek(0)
            return out_stream

    async def get_image_transcode_targets(self, src):
        return [ImageType.JPEG]
